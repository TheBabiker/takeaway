package com.example.takeaway;

import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import com.example.takeaway.Moduel.Applicant;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.UUID;

public class FormActivity extends AppCompatActivity {
    private Button submitBtn;
    private Handler handler;
    private Toast msg;

     CheckBox checkBox3;
     EditText emailET;
     EditText contactsNumET;
     EditText fullNameET;
     TextView dateOBBtn;

    DatabaseReference mReference;
    private int choice;
    List<String> nameofList;
    List<String> nameofImages;
    private DatePickerDialog.OnDateSetListener dateSetListener;
    private String DOB;
    private DatabaseReference reference;
    private String anwsers;


    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.contact_details);
        if (getIntent().getStringExtra("json") != null) {
            anwsers = getIntent().getStringExtra("json");
        }
        setViews();
        setFields();
        mReference = FirebaseDatabase.getInstance().getReference();
        DatabaseReference answerCollection = mReference.child("answerCollection");
        answerCollection.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                // This method is called once with the initial value and again
                // whenever data at this location is updated.
                // String value = dataSnapshot.getValue(String.class);
                choice  = (int) dataSnapshot.getChildrenCount();
                nameofList = new ArrayList<String>();
                nameofImages = new ArrayList<String>();
                for(DataSnapshot ds : dataSnapshot.getChildren()) {
                    String answerName = ds.getKey();
                    nameofList.add(answerName);
                    String imgeUrl = ds.child("imageUrl").getValue(String.class);
                    if(imgeUrl != null){
                        nameofImages.add(imgeUrl);
                    }
                }
            }

            @Override
            public void onCancelled(DatabaseError error) {
                // Failed to read value

            }
        });
    }

    private void setFields(){
        dateOBBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Calendar calendar = Calendar.getInstance();
                int year = calendar.get(Calendar.YEAR);
                int month = calendar.get(Calendar.MONTH);
                int day = calendar.get(Calendar.DAY_OF_MONTH);

                DatePickerDialog pickerDialog = new DatePickerDialog(FormActivity.this,
                        android.R.style.Theme_Holo_Light_Dialog_MinWidth,dateSetListener,year,month,day);
                pickerDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                pickerDialog.show();

            }
        });
        dateSetListener = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int year, int month, int day) {
                month = month+1;
                DOB = day + "/" + month + "/" + year;
                dateOBBtn.setText(DOB);

            }
        };
        submitBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (!checkBox3.isChecked()) {
                    Toast.makeText(FormActivity.this, "You have to agree with leaving your contact details", Toast.LENGTH_SHORT).show();
                } else if(fullNameET.getText().toString().isEmpty()){
                    Toast.makeText(FormActivity.this, "You have to fill your email address", Toast.LENGTH_SHORT).show();
                } else if(emailET.getText().toString().isEmpty()){
                    Toast.makeText(FormActivity.this, "You have to fill your contact", Toast.LENGTH_SHORT).show();
                } else if (contactsNumET.getText().toString().isEmpty()){
                    Toast.makeText(FormActivity.this, "You have to fill your full name", Toast.LENGTH_SHORT).show();
                }else if (dateOBBtn.getText().toString().isEmpty()) {
                    Toast.makeText(FormActivity.this, "You have to fill the dat of your birthday", Toast.LENGTH_SHORT).show();
                } else {
                    addApplicant();
                    msg.show();
                    handler.postDelayed(restartApp, 1000);
                }


            }
        });

    }

    public void setViews() {

        handler = new Handler();
        reference = FirebaseDatabase.getInstance().getReference("Applicants");
        msg = Toast.makeText(getApplicationContext(), "Submitted", Toast.LENGTH_SHORT);
        checkBox3 = findViewById(R.id.checkBox3);
        emailET = findViewById(R.id.emailET);
        contactsNumET = findViewById(R.id.contactsNumET);
        fullNameET = findViewById(R.id.fullNameET);
        dateOBBtn =   (TextView) findViewById(R.id.dateOfB);
        submitBtn = findViewById(R.id.submitBtn);


    }

    public void addApplicant() {

        String name = fullNameET.getText().toString();
        String email = emailET.getText().toString();
        Calendar calendar = Calendar.getInstance();
        String pattern = "yyyy-MM-dd";
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
        String formSubmissionDate = simpleDateFormat.format(new Date());
                /*DateFormat.getDateInstance(DateFormat.FULL).format(calendar.getTime());*/
        String quizeAnswers = "";
        String contactNum = contactsNumET.getText().toString();

        if (!TextUtils.isEmpty(name) && !TextUtils.isEmpty(email) && !TextUtils.isEmpty(DOB)) {
            Applicant applicant = new Applicant(name,email , DOB, formSubmissionDate,anwsers,contactNum);
            String id = UUID.randomUUID().toString();
            reference.child(id).setValue(applicant);
            fullNameET.setText("");
            emailET.setText("");
            DOB.equals("00" + "/" + "00" + "/" + "0000");
            formSubmissionDate = "";
            anwsers = "";

        }else {
            Toast.makeText(FormActivity.this, "Please fill all fields", Toast.LENGTH_LONG).show();
        }


    }

    @Override
    public void onBackPressed() {
        dialog();
    }

    private void dialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(FormActivity.this);
        builder.setMessage("Answers will lost, Do you want to continue?");
        builder.setTitle("Warning");
        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                Intent intent;
                intent = new Intent(FormActivity.this, QuizchallengeActivity.class);
                intent.putExtra("bestitz",getChoice() + "");
                intent.putExtra("NAMEOFANSWERS",(ArrayList<String>)getNameofList());
                intent.putExtra("NAMEOFIMAGES",(ArrayList<String>)getNameofImages());
                startActivity(intent);
            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        builder.create().show();
    }


    private Runnable restartApp = new Runnable() {
        @Override
        public void run() {
            Intent intent = new Intent(getApplicationContext(), WelcomeActivity.class);
            startActivity(intent);
        }
    };

    public int getChoice() {
        return choice;
    }

    public List<String> getNameofList() {
        return nameofList;
    }

    public List<String> getNameofImages() {
        return nameofImages;
    }
}
