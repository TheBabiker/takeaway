

/*
 * *
 *   Copyright (C) 2019
 *   Author : Ruikang Xu
 *
 *
 */

package com.example.takeaway.Adapter;

import android.content.Context;
import android.content.res.Resources;
import android.net.Uri;
import android.view.View;
import android.view.ViewGroup;
import androidx.annotation.NonNull;
import androidx.viewpager.widget.PagerAdapter;
import com.github.chrisbanes.photoview.PhotoView;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.squareup.picasso.Picasso;
import java.util.ArrayList;


public class CodeAdapter extends PagerAdapter {
    private int numOfQuiz;
    private String imageSource;
    FirebaseStorage storage;
    StorageReference storageRef;
    private ArrayList<String> namesofImages;

    public CodeAdapter(int count,ArrayList<String> arrayList) {
        numOfQuiz = count;
        setNamesofImages(arrayList);
        storage = FirebaseStorage.getInstance();
        storageRef = storage.getReference();
    }

    private void setNamesofImages(ArrayList<String> arrayList) {
        this.namesofImages = arrayList;
    }

    @Override public int getCount() {
        return numOfQuiz;
    }

    @Override public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view == object;
    }

    @Override
    public void destroyItem(@NonNull ViewGroup view, int position, @NonNull Object object) {
        view.removeView((View) object);
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup view, final int position) {
        final PhotoView imageView = new PhotoView(view.getContext());
        StorageReference gsReference = storage.getReferenceFromUrl(namesofImages.get(position));
        gsReference.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
            @Override
            public void onSuccess(Uri uri) {
                // Got the download URL for 'users/me/profile.png'
                String path = uri.toString();
                /// The string(file link) that you need
                imageSource = path;
                Picasso.get().load(imageSource).into(imageView);
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception exception) {
                // Handle any errors
            }
        });
        view.addView(imageView, ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.MATCH_PARENT);
        return imageView;
    }

    public void addItem() {
        numOfQuiz++;
        notifyDataSetChanged();
    }

    public void removeItem() {
        numOfQuiz--;
        numOfQuiz = numOfQuiz < 0 ? 0 : numOfQuiz;

        notifyDataSetChanged();
    }



    private int getResId(String name, Context context) {
        Resources r = context.getResources();
        int id = r.getIdentifier(name, "drawable", context.getPackageName());
        return id;
    }
}