

/*
 *
 *  * Copyright (C) 2019
 *  * Author : Ruikang Xu
 *  *
 *
 */

package com.example.takeaway.Adapter;
import android.graphics.Color;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.ScrollView;
import androidx.annotation.NonNull;
import androidx.viewpager.widget.PagerAdapter;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import java.util.ArrayList;

public class AnswerAdapter extends PagerAdapter {

    private int numOfQuiz;
    private DatabaseReference mReference;
    private final static int NUMOFANSWER = 6;
    private static int[] choice;
    public  boolean isAllHit;
    ArrayList<String> namesofQuiz;


    public AnswerAdapter(int count,ArrayList<String> arrayList) {
        numOfQuiz = count;
        this.namesofQuiz = arrayList;
        mReference = FirebaseDatabase.getInstance().getReference();
        DatabaseReference answerCollection = mReference.child("answerCollection");
        answerCollection.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                // This method is called once with the initial value and again
                // whenever data at this location is updated.
                // String value = dataSnapshot.getValue(String.class);
                choice  = new int[(int) dataSnapshot.getChildrenCount()];
            }

            @Override
            public void onCancelled(DatabaseError error) {
                // Failed to read value

            }
        });
    }

    public ArrayList<String> getNamesofQuiz() {
        return namesofQuiz;
    }

    @Override
    public int getCount() {
        return numOfQuiz;
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view == object;
    }

    @Override
    public void destroyItem(@NonNull ViewGroup view, int position, @NonNull Object object) {
        view.removeView((View) object);
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull  ViewGroup view,  int position) {
        mReference = FirebaseDatabase.getInstance().getReference();
        //通过键名，获取数据库实例对象的子节点对象
        ScrollView scrollView = new ScrollView(view.getContext());
        RadioGroup radioGroup = new RadioGroup(view.getContext());
        for (int i = 0; i < NUMOFANSWER; i++) {
            DatabaseReference  answer = mReference.child("answerCollection").child(getNamesofQuiz().get(position)).child("text" + String.valueOf(i));
            RadioButton textView = new RadioButton(view.getContext());
            readData(new MyCallback() {
                @Override
                public void onCallback(String value,RadioButton radioButton) {
                    radioButton.setText(value);
                }
            },answer,textView);
            textView.setGravity(Gravity.CENTER);
            textView.setTextColor(Color.WHITE);
            textView.setTextSize(48);
            textView.setId(i);
            textView.setOnClickListener(new MyClickListener(radioGroup,position));
            radioGroup.addView(textView);
        }
        radioGroup.setBackgroundColor(Color.TRANSPARENT);
        scrollView.addView(radioGroup);
        view.addView(scrollView, ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        return scrollView;
    }

    public void addItem() {
        numOfQuiz++;
        notifyDataSetChanged();
    }

    public interface MyCallback {
        void onCallback(String value,RadioButton radioButton);

    }

    public void readData(final MyCallback myCallback, DatabaseReference databaseReference, final RadioButton textView) {
        databaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                // This method is called once with the initial value and again
                // whenever data at this location is updated.
                String value = dataSnapshot.getValue(String.class);
                myCallback.onCallback(value,textView);
            }

            @Override
            public void onCancelled(DatabaseError error) {
                // Failed to read value

            }
        });
    }

    private class MyClickListener implements View.OnClickListener {
        private RadioGroup myParam;
        private int position;

        public MyClickListener(RadioGroup param,int pos) {
            myParam = param;
            position = pos;
        }

        @Override
        public void onClick(View view) {
            // do whatever you want
            int selectedId = myParam.getCheckedRadioButtonId();
            // find the radio button by returned id
            choice[position] = selectedId + 1;
            int isHit = 0;
            for(int i : choice){
                if(i == 0){
                    isHit++;
                }
            }
            if(isHit != 0){
                isAllHit = false;
            } else {
               isAllHit = true;
            }
        }
    }

    public  boolean isIsAllHit() {
        return isAllHit;
    }

    public  int[] getChoice() {
        return choice;
    }
}