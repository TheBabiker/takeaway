
/*
 *
 *  * Copyright (C) 2019
 *  * Author : Ruikang Xu
 *  *
 *
 */

package com.example.takeaway.Adapter;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Color;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.viewpager.widget.PagerAdapter;

import com.example.takeaway.R;
import com.github.chrisbanes.photoview.PhotoView;
import java.util.Random;

public class GalleryAdapter extends PagerAdapter {

    private final Random random = new Random();
    private int mSize;

    public GalleryAdapter() {
        mSize = 12;
    }


    @Override
    public int getCount() {
        return mSize;
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view == object;
    }

    @Override
    public void destroyItem(@NonNull ViewGroup view, int position, @NonNull Object object) {
        view.removeView((View) object);
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup view, int position) {
        PhotoView imageView = new PhotoView(view.getContext());
        TextView textView = new TextView(view.getContext());
        String[] numsAry = {"one", "two", "three", "four", "five", "six", "seven", "eight", "nine", "ten", "eleven"};

        if(position == mSize - 1){
            textView.setText(R.string.Code_challenge);
            textView.setBackgroundColor(0xff000000 | random.nextInt(0x00ffffff));
            textView.setGravity(Gravity.CENTER);
            textView.setTextColor(Color.WHITE);
            textView.setTextSize(48);
            view.addView(textView, ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.MATCH_PARENT);
            return textView;
        } else {
            for (int i = 0; i < numsAry.length; i++) {
                if (position == i) {
                    imageView.setImageResource(getResId(numsAry[i],view.getContext()));
                }
            }
            view.addView(imageView, ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.MATCH_PARENT);
            return imageView;
        }
    }

    public int getResId(String name, Context context) {
        Resources r = context.getResources();
        int id = r.getIdentifier(name, "drawable", context.getPackageName());
        return id;
    }
}