package com.example.takeaway;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AlertDialog;
import androidx.drawerlayout.widget.DrawerLayout;
import com.google.android.material.navigation.NavigationView;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import java.util.ArrayList;
import java.util.List;

public class AdminActivity extends Activity {
    private DrawerLayout drawerLayout;
    private ActionBarDrawerToggle barDrawerToggle;
    DatabaseReference mReference;
    private int choice;
    List<String> nameofList;
    List<String> nameofImages;
    NavigationView navigationView;
    ImageView logo;
    TextView numCandidates,numQuiz;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.admin_page);
        setLayout();
        setListener();

        if(getActionBar() != null){
            getActionBar().setDisplayHomeAsUpEnabled(true);
           // getSupportActionBar().setHomeButtonEnabled(true);
        }

        mReference = FirebaseDatabase.getInstance().getReference();
        DatabaseReference answerCollection = mReference.child("answerCollection");
        answerCollection.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                // This method is called once with the initial value and again
                // whenever data at this location is updated.
                // String value = dataSnapshot.getValue(String.class);
                choice  = (int) dataSnapshot.getChildrenCount();
                nameofList = new ArrayList<String>();
                nameofImages = new ArrayList<String>();
                for(DataSnapshot ds : dataSnapshot.getChildren()) {
                    String answerName = ds.getKey();
                    nameofList.add(answerName);
                    String imgeUrl = ds.child("imageUrl").getValue(String.class);
                    if(imgeUrl != null){
                        nameofImages.add(imgeUrl);
                    }
                }
            }

            @Override
            public void onCancelled(DatabaseError error) {
                // Failed to read value

            }
        });
    }

    private void setListener() {
       navigationView = (NavigationView) findViewById(R.id.adminNav);
       logo.setImageResource(R.drawable.logo2);
        navigationView.getHeaderView(0).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                drawerLayout.closeDrawer(navigationView);
                Toast.makeText(AdminActivity.this, "Header View is clicked!", Toast.LENGTH_SHORT).show();
            }
        });
        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.addQuize:
                        Log.d("test89489", "onOptionsItemSelected: sucess" );
                        Intent intent = new Intent(getBaseContext(), EditActivity.class);
                        Log.d("bestoot", "onClick: " + getChoice());
                        intent.putExtra("bestitz",getChoice() + "");
                        intent.putExtra("NAMEOFANSWERS",(ArrayList<String>)getNameofList());
                        intent.putExtra("NAMEOFIMAGES",(ArrayList<String>)getNameofImages());
                        startActivity(intent);
                        break;
                    case R.id.applicants:
                        Intent app = new Intent(getBaseContext(), ShowApplicantsActivity.class);
                        startActivity(app);
                        break;
                    case R.id.logOut:
                        onBackPressed();
                        break;
                }
                drawerLayout.closeDrawer(navigationView);
                return false;
            }
        });
        readNumQuiz(new MyCallback() {
            @Override
            public void passNumQuiz(int i) {
                numQuiz.setText("There're "+String.valueOf(i)+" Questions");
            }

            @Override
            public void passNumCandidates(int i) {

            }
        });

        readNumCandidates(new MyCallback() {
            @Override
            public void passNumQuiz(int i) {

            }

            @Override
            public void passNumCandidates(int i) {
                numCandidates.setText("There're "+String.valueOf(i) + " Candidates submitted");
            }
        });
    }

   /* public boolean onCreateOptionsMenu(Menu menu)
    {
        MenuInflater inflater = getMenuInflater();
        //设置menu界面为res/menu/menu.xml
        inflater.inflate(R.menu.menu_scrolling, menu);
        return true;
    }*/

    @Override
    public void onBackPressed() {
        dialog();
    }

    private void dialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(AdminActivity.this);
        builder.setMessage("Are you sure to log out?");
        builder.setTitle("WARNING");
        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                Intent intent = new Intent(AdminActivity.this,LoginActivity.class);
                startActivity(intent);
            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        builder.create().show();
    }

    public void setLayout() {
        drawerLayout = findViewById(R.id.drawer);
        barDrawerToggle = new ActionBarDrawerToggle(this, drawerLayout, R.string.open, R.string.close);
        drawerLayout.addDrawerListener(barDrawerToggle);
        barDrawerToggle.syncState();
        navigationView = findViewById(R.id.adminNav);
        logo =findViewById(R.id.logo);
        numCandidates = findViewById(R.id.numCandidates);
        numQuiz = findViewById(R.id.numQuiz);
    }


    public int getChoice() {
        return choice;
    }

    public List<String> getNameofList() {
        return nameofList;
    }

    public List<String> getNameofImages() {
        return nameofImages;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
      /*  if (barDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }
        return super.onOptionsItemSelected(item);*/
        Toast.makeText(this,"You click " + item.getTitle(), Toast.LENGTH_SHORT).show();
      switch (item.getItemId()){
          case R.id.addQuize:
              break;

              default:
                  break;

      }
      return true;
    }

    private interface MyCallback{
        void  passNumQuiz(int i);
        void  passNumCandidates(int i);
    }

    private void readNumQuiz(final MyCallback myCallback){
        mReference = FirebaseDatabase.getInstance().getReference();
        DatabaseReference answerCollection = mReference.child("answerCollection");
        answerCollection.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                // This method is called once with the initial value and again
                // whenever data at this location is updated.
                // String value = dataSnapshot.getValue(String.class);
               myCallback.passNumQuiz((int) dataSnapshot.getChildrenCount());
            }

            @Override
            public void onCancelled(DatabaseError error) {
                // Failed to read value

            }
        });
    }

    private void readNumCandidates(final MyCallback myCallback){
        mReference = FirebaseDatabase.getInstance().getReference();
        DatabaseReference answerCollection = mReference.child("Applicants");
        answerCollection.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                // This method is called once with the initial value and again
                // whenever data at this location is updated.
                // String value = dataSnapshot.getValue(String.class);
                myCallback.passNumCandidates((int) dataSnapshot.getChildrenCount());
            }

            @Override
            public void onCancelled(DatabaseError error) {
                // Failed to read value

            }
        });
    }

}
