
/*
 * *
 *   Copyright (C) 2019
 *   Author : Ruikang Xu
 *
 *
 */

package com.example.takeaway.Moduel;
public class AnswerModuel {


        private String text0;
        private String text1;
        private String text2;
        private String text3;
        private String text4;
        private String text5;
        private String imageUrl;
        private String[] texts;

        public  AnswerModuel(String[] arr,String imageUrl){
            this.texts = arr;
            for(int i = 0;i < texts.length; i ++){
              switch (i){
                  case 0:
                      setText0(texts[i]);
                      break;
                  case 1:
                      setText1(texts[i]);
                      break;
                  case 2:
                      setText2(texts[i]);
                      break;
                  case 3:
                      setText3(texts[i]);
                      break;
                  case 4:
                      setText4(texts[i]);
                      break;
                  case 5:
                      setText5(texts[i]);
                      break;
              }
            }
            this.imageUrl = imageUrl;
    }

        // Getter Methods

        public String getText0() {
            return text0;
        }

        public String getText1() {
            return text1;
        }

        public String getText2() {
            return text2;
        }

        public String getText3() {
            return text3;
        }

        public String getText4() {
            return text4;
        }

        public String getText5() {
            return text5;
        }

        public String getImageUrl() {
            return imageUrl;
        }

        // Setter Methods

        public void setText0(String text0) {
            this.text0 = text0;
        }

        public void setText1(String text1) {
            this.text1 = text1;
        }

        public void setText2(String text2) {
            this.text2 = text2;
        }

        public void setText3(String text3) {
            this.text3 = text3;
        }

        public void setText4(String text4) {
            this.text4 = text4;
        }

        public void setText5(String text5) {
            this.text5 = text5;
        }

        public void setImageUrl(String imageUrl) {
            this.imageUrl = imageUrl;
        }
}
